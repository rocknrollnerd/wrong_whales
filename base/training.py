from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
import os
import numpy as np
import lasagne
import theano

import config
from utils import (
    relative_squared_error,
    write_lasagne_model,
    read_lasagne_model
)


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


class Trainer(object):

    def __init__(self, weight_decay=1e-6, learning_rate=0.1, momentum=0.9, batch_size=20, epochs=100, split=0.05, lr_decay=1.):
        self.weight_decay = weight_decay
        self.learning_rate = theano.shared(np.array(learning_rate, dtype=theano.config.floatX))
        self.momentum = momentum
        self.batch_size = batch_size
        self.epochs = epochs
        self.split = split
        self.train_loss = []
        self.test_loss = []
        self.lr_decay = np.array(lr_decay, dtype=theano.config.floatX)

    def train(self, model, data, target):
        (
            data_train, data_test,
            target_train, target_test
        ) = train_test_split(data, target, test_size=self.split)

        print 'Training {} model on {} samples, test size {} samples'.format(
            model.name, data_train.shape[0], data_test.shape[0]
        )

        # training function
        self.prediction = lasagne.layers.get_output(model.network)
        # loss = lasagne.objectives.squared_error(prediction, self.target_var).mean()
        self.loss = relative_squared_error(self.prediction, model.target_var).mean()
        self.loss += self.weight_decay * lasagne.regularization.regularize_network_params(
            model.network, lasagne.regularization.l2
        )

        # create parameter update expressions
        self.params = lasagne.layers.get_all_params(model.network, trainable=True)
        self.updates = lasagne.updates.nesterov_momentum(
            self.loss, self.params,
            learning_rate=self.learning_rate,
            momentum=self.momentum
        )

        # compile training function that updates parameters and returns training loss
        self.train_fn = theano.function([model.input_var, model.target_var], self.loss, updates=self.updates)

        # actual training loop
        num_samples = data.shape[0]
        num_batches = num_samples // self.batch_size

        for epoch in range(self.epochs):
            try:
                tr_l = 0
                t_l = 0

                for input_batch, target_batch in iterate_minibatches(data_train, target_train, self.batch_size, shuffle=True):
                    bl = self.train_fn(input_batch, target_batch)
                    tr_l += bl

                tr_l /= num_batches
                t_l = model.val_fn(data_test, target_test)

                self.train_loss.append(tr_l)
                self.test_loss.append(t_l)

                self.learning_rate.set_value(self.learning_rate.get_value() * self.lr_decay)

                print("Epoch %d: Loss %g, test loss %g" % (epoch + 1, tr_l, t_l))
            except KeyboardInterrupt:
                break

        model.save()
        self.make_test_results(model, data_test, target_test)
        return model

    def make_test_results(self, model, data_test, target_test):
        results_path = os.path.join(config.ROOT, 'cache', '{}_test_results'.format(model.name))
        if not os.path.isdir(results_path):
            os.mkdir(results_path)

        random_idx = range(10)
        np.random.shuffle(random_idx)
        data_test = data_test[random_idx]
        target_test = target_test[random_idx]

        # load mean and std values
        datamean = np.load(os.path.join(config.ROOT, 'cache/datamean_{}.npy'.format(model.name, model.name)))
        datastd = np.load(os.path.join(config.ROOT, 'cache/datastd_{}.npy'.format(model.name, model.name)))

        for i, im in enumerate(data_test):
            ground = target_test[i]
            pred = model.predict(im)

            im *= datastd
            im += datamean

            # first image
            plt.subplot(1, 3, 1)
            plt.xticks(())
            plt.yticks(())
            c, h, w = im.shape
            plt.imshow(im.reshape(h, w, c), extent=[0, w, h, 0])
            plt.title('Original image')

            # then true mask
            plt.subplot(1, 3, 2)
            plt.xticks(())
            plt.yticks(())
            plt.imshow(ground.reshape((model.mask_shape[1], model.mask_shape[0])), cmap='gray')
            plt.title('Target mask')

            # then predicted mask
            pred_im = pred.reshape((model.mask_shape[1], model.mask_shape[0]))
            plt.subplot(1, 3, 3)
            plt.xticks(())
            plt.yticks(())
            plt.imshow(pred_im, cmap='gray')
            plt.title('Predicted mask')

            plt.savefig(os.path.join(results_path, '{}.png'.format(i)))
            plt.clf()
