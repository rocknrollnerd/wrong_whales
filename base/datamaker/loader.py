import os
import numpy as np
from PIL import Image
from skimage.exposure import equalize_adapthist

import config


class Loader(object):
    """
    Loads images and masks from files to numpy array,
    performs necessary normalizations and resizings.
    """

    def __init__(self, extractor, img_size, mask_size, cache_result=True):
        self.extractor = extractor
        self.img_size = img_size
        self.mask_size = mask_size
        self.dir = 'raw_datasets/{}'.format(self.name)
        self.img_dir = os.path.join(config.ROOT, self.dir, 'images')
        self.mask_dir = os.path.join(config.ROOT, self.dir, 'masks')
        self.cache_dir = os.path.join(config.ROOT, 'cache')
        self.cache_result = cache_result

    @property
    def data_path(self):
        return os.path.join(self.cache_dir, 'data_{}.npy'.format(self.name))

    @property
    def target_path(self):
        return os.path.join(self.cache_dir, 'target_{}.npy'.format(self.name))

    @property
    def datamean_path(self):
        return os.path.join(self.cache_dir, 'datamean_{}.npy'.format(self.name))

    @property
    def datastd_path(self):
        return os.path.join(self.cache_dir, 'datastd_{}.npy'.format(self.name))

    def load_dataset(self, return_intercepts=False, allow_cache=True):
        if allow_cache and os.path.isfile(self.data_path) and os.path.isfile(self.target_path):
            data = np.load(self.data_path)
            target = np.load(self.target_path)
            if (data.shape[-1] == self.img_size[0] and data.shape[-2] == self.img_size[1] and
                    target.shape[-1] == self.mask_size[0] * self.mask_size[1]):
                if not return_intercepts:
                    return data, target
                else:
                    datamean = np.load(self.datamean_path)
                    datastd = np.load(self.datastd_path)
                    return data, target, datamean, datastd

        # no cache, has extractor already extracted the stuff we need?
        lst1 = os.listdir(self.extractor.out_img_dir)
        lst2 = os.listdir(self.extractor.out_mask_dir)
        if len(lst1) == 0 or len(lst2) == 0:
            print 'Preprocessed images not found, running extractor'
            self.extractor.extract_annotations()

        images = sorted(os.listdir(self.img_dir), key=lambda x: int(x.split('.')[0]))
        masks = sorted(os.listdir(self.mask_dir), key=lambda x: int(x.split('.')[0]))

        data = np.zeros((len(images), self.img_size[1], self.img_size[0], 3))
        target = []

        for i, (im, mask) in enumerate(zip(images, masks)):
            impath = os.path.join(self.img_dir, im)
            maskpath = os.path.join(self.mask_dir, mask)
            data[i, ...] = equalize_adapthist(self.load_image(impath))
            mask = self.load_mask(maskpath).ravel()
            target.append(mask)

        target = np.vstack(target)

        n, h, w, c = data.shape

        data = np.float32(data)
        target = np.float32(target)
        data = data.reshape(n, c, h, w)

        eplison = 1e-6
        datamean = np.mean(data, axis=0)
        np.save(self.datamean_path, datamean)
        data -= datamean
        datastd = np.std(data, axis=0)
        np.save(self.datastd_path, datastd)
        data /= (datastd + eplison)

        if self.cache_result:
            np.save(self.data_path, data)
            np.save(self.target_path, target)

        if return_intercepts:
            return data, target, datamean, datastd
        else:
            return data, target

    def load_image(self, path):
        im = Image.open(path)
        w, h = im.size
        downsize_factor = (
            w / float(self.img_size[0]),
            h / float(self.img_size[1])
        )
        im.thumbnail((int(w / downsize_factor[0]), int(h / downsize_factor[1])), Image.ANTIALIAS)
        im = np.float32(np.asarray(im)) / 255.
        return im

    def load_mask(self, path):
        im = Image.open(path)
        w, h = im.size
        downsize_factor = (
            w / float(self.mask_size[0]),
            h / float(self.mask_size[1])
        )
        im.thumbnail((int(w / downsize_factor[0]), int(h / downsize_factor[1])), Image.ANTIALIAS)
        im = np.float32(np.asarray(im)) / 255.
        if len(im.shape) > 2:
            im = np.mean(im, axis=2)
        im[im < 0.5] = 0.
        im[im >= 0.5] = 1.
        return im
