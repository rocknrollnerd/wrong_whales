import os

import config
from utils import (
    write_lasagne_model,
    read_lasagne_model
)


class Model(object):

    def predict(self, data):
        if len(data.shape) == 3:
            data = data.reshape(1, *data.shape)
        return self.predict_fn(data)

    def save(self):
        write_lasagne_model(self.network, self.name)

    def load(self):
        read_lasagne_model(self.network, self.name)

    def is_ready(self):
        name = '.'.join([self.name, 'model'])
        path = os.path.join(config.ROOT, 'cache', name)
        return os.path.isfile(path)
