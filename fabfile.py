from fabric.api import *
from fabric.operations import local, get

env.hosts = ['ec2-50-16-78-7.compute-1.amazonaws.com']
env.user = 'ubuntu'
env.key_filename = '/home/anglerfish/.ssh/laptop.pem'
env.path = '/mnt/whales2/wrong_whales'


def deploy():
    with cd(env.path):
        run('git pull origin master')

