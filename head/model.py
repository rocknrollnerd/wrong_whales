import lasagne
from lasagne.nonlinearities import leaky_rectify
import theano
import theano.tensor as T

from base.model import Model
from utils import relative_squared_error


class HeadModel(Model):
    name = 'head'

    def __init__(self, input_shape, mask_shape):
        self.input_var = T.tensor4('X')
        self.target_var = T.matrix('y')
        self.input_shape = input_shape
        self.mask_shape = mask_shape

        network = lasagne.layers.InputLayer((None, 3, input_shape[1], input_shape[0]), self.input_var)

        network = lasagne.layers.dropout(network, 0.2)
        network = lasagne.layers.Conv2DLayer(network, 16, (7, 7), nonlinearity=leaky_rectify)
        network = lasagne.layers.Conv2DLayer(network, 16, (7, 7), nonlinearity=leaky_rectify)
        network = lasagne.layers.dropout(network, 0.25)
        network = lasagne.layers.Pool2DLayer(network, (2, 2), mode='max')
        network = lasagne.layers.Conv2DLayer(network, 32, (5, 5), nonlinearity=leaky_rectify)
        network = lasagne.layers.Conv2DLayer(network, 32, (5, 5), nonlinearity=leaky_rectify)
        network = lasagne.layers.dropout(network, 0.25)
        network = lasagne.layers.Pool2DLayer(network, (2, 2), mode='max')

        # network = lasagne.layers.dropout(network, 0.2)
        # network = lasagne.layers.Conv2DLayer(network, 32, (3, 3), nonlinearity=leaky_rectify)
        # network = lasagne.layers.Conv2DLayer(network, 32, (3, 3), nonlinearity=leaky_rectify)
        # network = lasagne.layers.dropout(network, 0.25)
        # network = lasagne.layers.Pool2DLayer(network, (2, 2), mode='max')
        # network = lasagne.layers.Conv2DLayer(network, 64, (3, 3), nonlinearity=leaky_rectify)
        # network = lasagne.layers.Conv2DLayer(network, 64, (3, 3), nonlinearity=leaky_rectify)
        # network = lasagne.layers.dropout(network, 0.25)
        # network = lasagne.layers.Pool2DLayer(network, (2, 2), mode='max')
        # network = lasagne.layers.Conv2DLayer(network, 128, (3, 3), nonlinearity=leaky_rectify)
        # # network = lasagne.layers.Conv2DLayer(network, 128, (3, 3), nonlinearity=leaky_rectify)
        # network = lasagne.layers.dropout(network, 0.25)
        # network = lasagne.layers.Pool2DLayer(network, (2, 2), mode='max')

        # network = lasagne.layers.dropout(network, 0.25)
        # network = lasagne.layers.DenseLayer(network, 128, nonlinearity=leaky_rectify)
        # network = lasagne.layers.dropout(network, 0.5)
        network = lasagne.layers.DenseLayer(network, 2048, nonlinearity=leaky_rectify)
        network = lasagne.layers.dropout(network, 0.5)
        network = lasagne.layers.DenseLayer(network, 2048, nonlinearity=leaky_rectify)
        network = lasagne.layers.dropout(network, 0.5)
        network = lasagne.layers.DenseLayer(network, mask_shape[0] * mask_shape[1], nonlinearity=leaky_rectify)

        self.network = network

        self.test_prediction = lasagne.layers.get_output(self.network, deterministic=True)
        self.test_loss = relative_squared_error(self.test_prediction, self.target_var).mean()
        self.val_fn = theano.function([self.input_var, self.target_var], self.test_loss)
        self.predict_fn = theano.function([self.input_var], self.test_prediction)

        # 0.067
