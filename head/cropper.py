import os
import numpy as np
from PIL import Image
import copy
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from sklearn.cluster import DBSCAN

import config


class HeadCropper(object):
    name = 'head'
    MASK_H = config.MASK_HEAD_SHAPE[1]
    MASK_W = config.MASK_HEAD_SHAPE[0]

    def __init__(self, model, extractor, img_dir, out_dir):
        self.model = model
        self.img_dir = img_dir
        self.out_dir = out_dir
        self.extractor = extractor
        self.cache_dir = os.path.join(config.ROOT, 'cache')

    @property
    def data_path(self):
        return os.path.join(self.cache_dir, 'data_{}.npy'.format(self.name))

    @property
    def target_path(self):
        return os.path.join(self.cache_dir, 'target_{}.npy'.format(self.name))

    @property
    def datamean_path(self):
        return os.path.join(self.cache_dir, 'datamean_{}.npy'.format(self.name))

    @property
    def datastd_path(self):
        return os.path.join(self.cache_dir, 'datastd_{}.npy'.format(self.name))

    def make_prediction_mask(self, pil_im, datamean, datastd):
        orig_w, orig_h = pil_im.size
        res_im = pil_im.resize((self.extractor.OUT_W, self.extractor.OUT_H), Image.ANTIALIAS)
        im = np.float32(np.asarray(res_im)) / 255.
        h, w, c = im.shape
        im = im.reshape(1, c, h, w)
        im -= datamean
        im /= (datastd + 1e-6)
        im = np.float32(im)

        # predict
        pred = self.model.predict(im)[0].reshape(self.MASK_H, self.MASK_W)
        return pred

    def clusterize_filter(self, xs, ys):
        pts = np.vstack(zip(xs, ys))
        # use DBSCAN to find connected regions
        db = DBSCAN(eps=3, min_samples=5).fit(pts)
        labels = db.labels_
        unique_labels = set(labels)
        if len(unique_labels) == 1:
            # one cluster, nothing to worry about
            filtered = pts[labels == list(unique_labels)[0]]
            return filtered[:, 0], filtered[:, 1]
        # else... problem
        # have to decide between multiple clusters

        # first: are some masks bigger then the others?
        clusters = {}
        for k in unique_labels:
            clusters[k] = len(pts[labels == k])
        max_num_pts = np.max(clusters.values())
        filtered_labels = []
        for label, num_pts in clusters.iteritems():
            if num_pts / float(max_num_pts) > 0.7:
                filtered_labels.append(label)

        if len(filtered_labels) == 1:
            filtered = pts[labels == filtered_labels[0]]
            return filtered[:, 0], filtered[:, 1]
        else:
            # if still multiple maskregions are available, we need to do something
            # ideas:
            # 1) simple: extract label/region at random.
            # 2) less simple: allow multiple possible candidates for whale head, but what's next?
            #    how do we decide which is to use/which matters more?
            # 3) get multiple sub-crops of the image: maybe that'll help?

            # let's go random for now
            selected_label = np.random.choice(filtered_labels)
            filtered = pts[labels == selected_label]
            return filtered[:, 0], filtered[:, 1]

        # colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
        # for k, col in zip(unique_labels, colors):
        #     if k == -1:
        #         continue
        #     label_mask = (labels == k)
        #     subpts = pts[label_mask]
        #     plt.scatter(subpts[:, 0], subpts[:, 1], c=col, s=50)
        # plt.xlim((0, config.INPUT_HEAD_SHAPE[0]))
        # plt.ylim((0, config.INPUT_HEAD_SHAPE[1]))
        # # plt.gca().set_aspect('equal', 'datalim')
        # plt.show()
        # return xs, ys

    def fit_bbox(self, mask):
        # first binarize prediction mask
        mask[mask < 0.2] = 0
        mask[mask >= 0.2] = 1

        # then turn it to a collection of pts
        xs = []
        ys = []
        for i in xrange(mask.shape[0]):
            for j in xrange(mask.shape[1]):
                if mask[i, j] == 1:
                    ys.append(i)
                    xs.append(j)

        xs, ys = self.clusterize_filter(xs, ys)

        # for now let's forget clustering. find mean and dx, dy sides
        ox = np.mean(xs)
        oy = np.mean(ys)
        w = np.max(xs) - np.min(xs)
        h = np.max(ys) - np.min(ys)

        # remember resizing in `make_prediction_mask2`? unroll it by using `resize_factors`
        # w *= resize_factors[0]
        # h *= resize_factors[1]
        return ox - (w / 2), oy - (h / 2), w, h

    def run(self, batch_size, n_random):
        # prepare a list of files
        files = sorted(os.listdir(self.img_dir))

        # if `n_random` is chosed, take n random images from the source dir
        if n_random:
            files = np.random.choice(files, n_random)

        # create output dir if nonexistent
        if not os.path.isdir(self.out_dir):
            os.mkdir(self.out_dir)

        c = 0
        datamean = np.load(self.datamean_path)
        datastd = np.load(self.datastd_path)
        for i, fname in enumerate(files):
            path = os.path.join(self.img_dir, fname)
            ext = path.split('.')[-1]
            original_pil_img = Image.open(path)
            orig_w, orig_h = original_pil_img.size
            resized_img = copy.deepcopy(original_pil_img)

            mask = self.make_prediction_mask(resized_img, datamean, datastd)
            bbox = self.fit_bbox(mask)

            plt.subplot(1, 4, 1)
            plt.imshow(np.asarray(original_pil_img))

            plt.subplot(1, 4, 2)
            plt.imshow(mask, cmap='gray')

            plt.subplot(1, 4, 3)
            plt.imshow(np.zeros_like(mask))
            plt.gca().add_patch(Rectangle((bbox[0], bbox[1]), bbox[2], bbox[3], lw=2, color='#19F700', fill=False))

            # now resize the bbox to a full size
            bx, by, bw, bh = bbox

            res_w = orig_w / (self.MASK_W)
            res_h = orig_h / (self.MASK_H)

            bx *= res_w
            by *= res_h
            bw *= res_w
            bh *= res_h

            left, upper, right, lower = int(bx), int(by), int(bx + bw), int(by + bh)
            cropped = original_pil_img.crop((left, upper, right, lower))
            # make cropped thingy square
            cropped = cropped.resize((100, 100), Image.ANTIALIAS)

            # and then rotate it to a common orientation
            ox = bx + bw / 2.
            if ox < orig_w / 2.:
                # head looks to the left
                cropped = cropped.rotate(-90)
            else:
                # head looks to the right
                cropped = cropped.rotate(90)

            # aaaand save
            c += 1
            print c, 'of', len(files), fname
            # cropped.save(os.path.join(self.out_dir, fname))

            plt.subplot(1, 4, 4)
            plt.imshow(np.asarray(cropped))
            plt.show()

            # plt.savefig(os.path.join(self.out_dir, fname))
            plt.clf()
