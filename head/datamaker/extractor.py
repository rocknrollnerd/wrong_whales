import os
from PIL import Image, ImageDraw
import numpy as np
import matplotlib.pyplot as plt
import json

from head.datamaker.augmentation import augment
import config

"""
Does the datamaking and ties together
all the processing performed with the initial
dataset: loading, resizing, drawing masks, augmentations
"""


class HeadExtractor(object):
    OUT_H = config.INPUT_HEAD_SHAPE[1]
    OUT_W = config.INPUT_HEAD_SHAPE[0]
    name = 'head'
    OUT_DIR = 'raw_datasets/{}'.format(name)

    def __init__(self):
        self.img_dir = config.DATASET_HEAD
        self.out_img_dir = os.path.join(config.ROOT, self.OUT_DIR, 'images')
        self.out_mask_dir = os.path.join(config.ROOT, self.OUT_DIR, 'masks')

    def fit_bbox_to_square(self, x, y, w, h):
        ox = x + w / 2.
        oy = y + h / 2.
        side = w if w > h else h
        x_new = ox - side / 2.
        y_new = oy - side / 2.
        return int(x_new), int(y_new), int(side), int(side)

    def scale_image_size(self, w, h):
        # scale h to OUT_H
        return w * self.OUT_H / h, self.OUT_H

    def pad_width(self, pil_im, bbox, pad_size=None):
        x, y, bbox_w, bbox_h = bbox
        w, h = pil_im.size
        # if pad_size not specified, pad to OUT_W - w / 2
        if not pad_size:
            pad_size = int((self.OUT_W - w) / 2.)

        # pad zeros
        im = np.asarray(pil_im)
        x += pad_size
        im = np.pad(im, ((0, 0), (pad_size, pad_size), (0, 0)), 'constant', constant_values=0)
        w += pad_size * 2

        pil_im = Image.fromarray(im)
        return pil_im, (x, y, bbox_w, bbox_h)

    def get_mask(self, bbox):
        x, y, bbox_w, bbox_h = bbox
        im = Image.new('RGB', (self.OUT_W, self.OUT_H), (0, 0, 0))
        d = ImageDraw.Draw(im)
        d.rectangle([(x, y), (x + bbox_w, y + bbox_h)], fill=(255, 255, 255))

        return im

        np_im = np.float32(np.asarray(im)) / 255.
        # find out how much of a bbox is displayed
        # if less then 70%, paint an entirely black mask to protect the integrity of the box
        max_mask_size = bbox_w * bbox_h
        real_mask_size = np.count_nonzero(np_im > 0.5) / 3.  # channels
        if real_mask_size / float(max_mask_size) < 0.7:
            np_im *= 0
            im = Image.fromarray(np.uint8(np_im))
        return im

    def crop_image(self, pil_im, bbox):
        """
        first reshaping to OUT_H (leaving width proportional)
        Then cropping the image the way that it becomes exactly
        (OUT_H, OUT_W).

        In case when the width is less then OUT_W, the image is padded with black.
        """
        # first resize
        orig_w, orig_h = pil_im.size
        w, h = self.scale_image_size(*pil_im.size)
        x, y, bbox_w, bbox_h = bbox
        x /= (orig_w / float(w))
        bbox_w /= (orig_w / float(w))
        y /= (orig_h / float(h))
        bbox_h /= (orig_h / float(h))
        pil_im.thumbnail((w, h), Image.ANTIALIAS)
        # the do we need to pad it?
        if w < self.OUT_W:
            # leave some place for random cropping
            pad_size = int(((self.OUT_W - w) / 2.) * 1.3)
            pil_im, bbox = self.pad_width(pil_im, bbox, pad_size)
            w += pad_size * 2

        # some random cropping! head is fixed
        crops = []
        delta_space = w - self.OUT_W
        for i in xrange(min(10, delta_space)):
            delta = np.random.randint(0, delta_space)
            left, upper, right, lower = delta, 0, delta + self.OUT_W, self.OUT_H
            cropped = pil_im.crop((left, upper, right, lower))
            crops.append((
                cropped,
                (x - delta, y, bbox_w, bbox_h)
            ))
        return crops

        # # time for cropping. height is fixed, so we only choose `x` position
        # # the idea is that we're looking at the spatial pattern of a head being
        # # somewhere right or somewhere left
        # ox = x + (bbox_w / 2.)
        # if ox < w / 2.:
        #     # left size
        #     left, upper, right, lower = 0, 0, self.OUT_W, self.OUT_H
        #     # bbox left unchanged
        # else:
        #     left, upper, right, lower = w - self.OUT_W, 0, w, self.OUT_H
        #     # don't forget to adjust bbox!
        #     x -= (w - self.OUT_W)
        # cropped = pil_im.crop((left, upper, right, lower))
        # return cropped, (x, y, bbox_w, bbox_h)

    def rescale_bbox(self, bbox, orig_size, new_size):
        orig_w, orig_h = orig_size
        new_w, new_h = new_size
        x, y, bbox_w, bbox_h = bbox
        x /= (orig_w / float(new_w))
        bbox_w /= (orig_w / float(new_w))
        y /= (orig_h / float(new_h))
        bbox_h /= (orig_h / float(new_h))
        return x, y, bbox_w, bbox_h

    def extract_annotations(self, verbose=True):
        c = 0
        with open(config.ANNOTATIONS_HEAD, 'r') as f:
            annotations = json.loads(f.read())
        for data in annotations[::-1]:
            fn = data['filename']
            ffn = fn.split('/')[-1]
            ext = ffn.split('.')[-1]
            path = os.path.join(self.img_dir, ffn)
            fn = fn[3:] if fn.startswith('../') else fn
            if verbose:
                print 'Processing {}'.format(fn)
            if data['annotations']:
                bbox = data['annotations'][0]
                bbox = self.fit_bbox_to_square(bbox['x'], bbox['y'], bbox['width'], bbox['height'])
                # left, upper, right, lower = int(x), int(y), int(x + w), int(y + h)
                pil_im = Image.open(path)
                orig_size = pil_im.size
                pil_im = pil_im.resize((self.OUT_W, self.OUT_H), Image.ANTIALIAS)
                bbox = self.rescale_bbox(bbox, orig_size, (self.OUT_W, self.OUT_H))
                # crops = self.crop_image(pil_im, bbox)
                # for cropped, bbox in crops:
                mask = self.get_mask(bbox)
                augmentations = augment(pil_im, mask)
                for im, mask in augmentations:
                    # plt.subplot(1, 2, 1)
                    # plt.imshow(np.asarray(im))
                    # plt.subplot(1, 2, 2)
                    # plt.imshow(np.asarray(mask))
                    # plt.show()

                    im.save(os.path.join(self.out_img_dir, '{}.{}'.format(c, ext)))
                    mask.save(os.path.join(self.out_mask_dir, '{}.{}'.format(c, ext)))
                    c += 1
