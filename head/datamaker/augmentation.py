from PIL import Image


def augment(pil_im, pil_mask):
    im1 = pil_im
    mask1 = pil_mask

    im2 = pil_im.transpose(Image.FLIP_LEFT_RIGHT)
    mask2 = pil_mask.transpose(Image.FLIP_LEFT_RIGHT)

    im3 = pil_im.transpose(Image.FLIP_TOP_BOTTOM)
    mask3 = pil_mask.transpose(Image.FLIP_TOP_BOTTOM)

    im4 = im2.transpose(Image.FLIP_TOP_BOTTOM)
    mask4 = mask2.transpose(Image.FLIP_TOP_BOTTOM)

    return (
        (im1, mask1),
        (im2, mask2),
        (im3, mask3),
        (im4, mask4),
    )
