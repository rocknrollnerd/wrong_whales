import os
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

import config


class Flipper(object):
    name = 'orientation'

    def __init__(self, model, extractor, img_dir, out_dir):
        self.model = model
        self.img_dir = img_dir
        self.out_dir = out_dir
        self.extractor = extractor
        self.cache_dir = os.path.join(config.ROOT, 'cache')

    @property
    def datamean_path(self):
        return os.path.join(self.cache_dir, 'datamean_{}.npy'.format(self.name))

    @property
    def datastd_path(self):
        return os.path.join(self.cache_dir, 'datastd_{}.npy'.format(self.name))

    def run(self, batch_size, n_random):
        # prepare a list of files
        files = sorted(os.listdir(self.img_dir))

        # if `n_random` is chosed, take n random images from the source dir
        if n_random:
            files = np.random.choice(files, n_random)

        # create output dir if nonexistent
        if not os.path.isdir(self.out_dir):
            os.mkdir(self.out_dir)

        num_batches = len(files) / batch_size + 1

        datamean = np.load(self.datamean_path)
        datastd = np.load(self.datastd_path)

        for b in xrange(num_batches):
            batch_data = np.zeros((batch_size, config.INPUT_HEAD_SHAPE[1], config.INPUT_HEAD_SHAPE[0], 3))
            batch_fnames = []
            batch_originals = []
            for i, fname in enumerate(files[b * batch_size: (b + 1) * batch_size]):
                batch_fnames.append(fname)
                path = os.path.join(self.img_dir, fname)
                original_pil_img = Image.open(path)
                batch_originals.append(original_pil_img)
                res_im = original_pil_img.resize((self.extractor.OUT_W, self.extractor.OUT_H), Image.ANTIALIAS)

                im = np.float32(np.asarray(res_im)) / 255.
                batch_data[i] = im

            batch_data = batch_data.reshape(batch_data.shape[0], 3, config.INPUT_HEAD_SHAPE[1], config.INPUT_HEAD_SHAPE[0])
            batch_data -= datamean
            batch_data /= (datastd + 1e-6)
            batch_data = np.float32(batch_data)

            preds = self.model.predict(batch_data)
            for i in xrange(batch_size):
                if i >= len(batch_originals):
                    break
                pred = preds[i]
                fname = batch_fnames[i]
                original_pil_img = batch_originals[i]
                if pred == 0:
                    # left. nothing to do
                    pass
                elif pred == 1:
                    # right. need to flip
                    original_pil_img = original_pil_img.transpose(Image.FLIP_LEFT_RIGHT)
                elif pred == 2:
                    original_pil_img = None
                    print 'Bad image', fname
                print 'orientation', pred
                plt.imshow(np.asarray(original_pil_img))
                plt.show()
                # if original_pil_img:
                #     original_pil_img.save(os.path.join(self.out_dir, fname))
