from PIL import Image


def augment(pil_im, bbox):
    w, h = pil_im.size
    x, y, bbox_w, bbox_h = bbox
    im1 = pil_im
    bbox1 = x, y, bbox_w, bbox_h

    im2 = pil_im.transpose(Image.FLIP_LEFT_RIGHT)
    bbox2 = w - x - bbox_w, y, bbox_w, bbox_h

    im3 = pil_im.transpose(Image.FLIP_TOP_BOTTOM)
    bbox3 = x, h - y - bbox_h, bbox_w, bbox_h

    im4 = im2.transpose(Image.FLIP_TOP_BOTTOM)
    bbox4 = w - x - bbox_w, h - y - bbox_h, bbox_w, bbox_h

    return (
        (im1, bbox1),
        (im2, bbox2),
        (im3, bbox3),
        (im4, bbox4),
    )
