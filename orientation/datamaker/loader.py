import os
import numpy as np
from skimage.exposure import equalize_adapthist

from base.datamaker.loader import Loader
import config


class OrientationLoader(Loader):
    name = 'orientation'

    def __init__(self, extractor, img_size, cache_result=True):
        self.extractor = extractor
        self.img_size = img_size
        self.dir = 'raw_datasets/{}'.format(self.name)
        self.img_dir = os.path.join(config.ROOT, self.dir, 'images')
        self.cache_dir = os.path.join(config.ROOT, 'cache')
        self.cache_result = cache_result

    def load_dataset(self, return_intercepts=False, allow_cache=True):
        if allow_cache and os.path.isfile(self.data_path) and os.path.isfile(self.target_path):
            data = np.load(self.data_path)
            target = np.load(self.target_path)
            if (data.shape[-1] == self.img_size[0] and data.shape[-2] == self.img_size[1] and
                    target.shape[-1] == self.mask_size[0] * self.mask_size[1]):
                if not return_intercepts:
                    return data, target
                else:
                    datamean = np.load(self.datamean_path)
                    datastd = np.load(self.datastd_path)
                    return data, target, datamean, datastd

        # no cache, has extractor already extracted the stuff we need?
        if os.path.isfile(os.path.join(self.cache_dir, 'orientations.npy')):
            print 'Orientations not extracted, running extractor'
            self.extractor.extract_annotations()

        images = sorted(os.listdir(self.img_dir), key=lambda x: int(x.split('.')[0]))
        orientations = np.load(os.path.join(self.cache_dir, 'orientations.npy'))

        data = np.zeros((len(images), self.img_size[1], self.img_size[0], 3))
        target = []

        for i, im in enumerate(images):
            impath = os.path.join(self.img_dir, im)
            data[i, ...] = equalize_adapthist(self.load_image(impath))
            target.append(orientations[i])

        target = np.array(target)

        n, h, w, c = data.shape

        data = np.float32(data)
        target = np.float32(target)
        data = data.reshape(n, c, h, w)

        eplison = 1e-6
        datamean = np.mean(data, axis=0)
        np.save(self.datamean_path, datamean)
        data -= datamean
        datastd = np.std(data, axis=0)
        np.save(self.datastd_path, datastd)
        data /= (datastd + eplison)

        if self.cache_result:
            np.save(self.data_path, data)
            np.save(self.target_path, target)

        if return_intercepts:
            return data, target, datamean, datastd
        else:
            return data, target
