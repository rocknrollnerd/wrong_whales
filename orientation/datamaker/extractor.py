import os
from PIL import Image, ImageDraw
import numpy as np
import matplotlib.pyplot as plt
import json
from matplotlib.patches import Rectangle

from orientation.datamaker.augmentation import augment
import config


class OrientationExtractor(object):
    OUT_H = config.INPUT_HEAD_SHAPE[1]
    OUT_W = config.INPUT_HEAD_SHAPE[0]
    name = 'head'
    OUT_DIR = 'raw_datasets/{}'.format(name)

    def __init__(self):
        self.img_dir = config.DATASET_HEAD
        self.out_img_dir = os.path.join(config.ROOT, self.OUT_DIR, 'images')
        self.out_mask_dir = os.path.join(config.ROOT, self.OUT_DIR, 'masks')

    def fit_bbox_to_square(self, x, y, w, h):
        ox = x + w / 2.
        oy = y + h / 2.
        side = w if w > h else h
        x_new = ox - side / 2.
        y_new = oy - side / 2.
        return int(x_new), int(y_new), int(side), int(side)

    def estimate_orientation(self, pil_im, bbox):
        x, y, bbox_w, bbox_h = bbox
        w, h = pil_im.size
        if x < w / 2.:
            # left
            orientation = 0
            border_closeness = x / float(w)
        else:
            # right
            orientation = 1
            border_closeness = (w - x - bbox_w) / float(w)
        if border_closeness > 0.3:
            # head to close to center of the image, consider that a `bad` cropping.
            orientation = 2
        return orientation

    def extract_annotations(self, verbose=True):
        c = 0
        with open(config.ANNOTATIONS_HEAD, 'r') as f:
            annotations = json.loads(f.read())
        orientations = []
        for data in annotations[::-1]:
            fn = data['filename']
            ffn = fn.split('/')[-1]
            ext = ffn.split('.')[-1]
            path = os.path.join(self.img_dir, ffn)
            fn = fn[3:] if fn.startswith('../') else fn
            if verbose:
                print 'Processing {}'.format(fn)
            if data['annotations']:
                bbox = data['annotations'][0]
                bbox = self.fit_bbox_to_square(bbox['x'], bbox['y'], bbox['width'], bbox['height'])
                pil_im = Image.open(path)
                augmentations = augment(pil_im, bbox)
                for im, subbbox in augmentations:
                    o = self.estimate_orientation(im, subbbox)
                    x, y, bbox_w, bbox_h = subbbox
                    # plt.imshow(np.asarray(im))
                    # plt.gca().add_patch(Rectangle((x, y), bbox_w, bbox_h, lw=2, color='y', fill=False))
                    # plt.show()
                    orientations.append(o)
        orientations = np.array(orientations)
        np.save(os.path.join(config.ROOT, 'cache', 'orientations.npy'))
