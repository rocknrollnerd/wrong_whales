import optparse
from pipeline import Pipeline


if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option('--body', action="store_true", dest='body', default=False)
    parser.add_option('--head', action="store_true", dest='head', default=False)

    options, remainder = parser.parse_args()

    if not options.head and not options.body:
        print 'Nothing to train, set --head, --body or both'

    pipeline = Pipeline()
    if options.body:
        pipeline.train_body()
    if options.head:
        pipeline.train_head()
