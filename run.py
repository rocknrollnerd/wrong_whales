import optparse

from pipeline import Pipeline


if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option('--body', action="store_true", dest='body', default=False)
    parser.add_option('--head', action="store_true", dest='head', default=False)
    parser.add_option('--classify', action="store_true", dest='head', default=False)
    parser.add_option('-i', action="store", dest="img_dir")
    parser.add_option('-o', action="store", dest="out_dir")
    parser.add_option('--n_random', action="store", type="int", dest="n_random", default=0)
    parser.add_option('--batch_size', action="store", type="int", dest="batch_size", default=20)

    options, remainder = parser.parse_args()

    pipeline = Pipeline()
    if options.body:
        pipeline.run_body(options.img_dir, options.out_dir, options.batch_size, options.n_random)

    if options.head:
        pipeline.run_head(options.img_dir, options.out_dir, options.batch_size, options.n_random)
