import os

import config
from base.training import Trainer

from body.model import BodyModel
from body.datamaker.loader import BodyLoader
from body.datamaker.extractor import BodyExtractor
from body.cropper import BodyCropper

from head.model import HeadModel
from head.datamaker.loader import HeadLoader
from head.datamaker.extractor import HeadExtractor
from head.cropper import HeadCropper

from orientation.model import OrientationModel
from orientation.datamaker.loader import OrientationLoader
from orientation.datamaker.extractor import OrientationExtractor


class Pipeline(object):
    body_model = BodyModel(config.INPUT_BODY_SHAPE, config.MASK_BODY_SHAPE)
    body_loader = BodyLoader(BodyExtractor(), config.INPUT_BODY_SHAPE, config.MASK_BODY_SHAPE)
    body_trainer = Trainer()

    head_model = HeadModel(config.INPUT_HEAD_SHAPE, config.MASK_HEAD_SHAPE)
    head_loader = HeadLoader(HeadExtractor(), config.INPUT_HEAD_SHAPE, config.MASK_HEAD_SHAPE)
    head_trainer = Trainer(learning_rate=0.1, lr_decay=0.999)

    orientation_model = OrientationModel(config.INPUT_HEAD_SHAPE)
    orientation_loader = OrientationLoader(OrientationExtractor(), config.INPUT_HEAD_SHAPE)
    orientation_trainer = Trainer()

    def __init__(self):
        pass

    def is_ready(self):
        return self.body_model.is_ready() and self.head_model.is_ready()

    def train_body(self):
        print 'Training body model...'
        data, target = self.body_loader.load_dataset()
        self.body_trainer.train(self.body_model, data, target)
        print 'Body model ready'

    def train_head(self):
        print 'Training head model...'
        data, target = self.head_loader.load_dataset()
        self.head_trainer.train(self.head_model, data, target)
        print 'Head model ready'

    def train_orientation(self):
        print 'Training orientation model...'
        data, target = self.orientation_loader.load_dataset()
        self.orientation_trainer.train(self.orientation_model, data, target)
        print 'Orientation model ready'

    def train(self):
        self.train_body()
        self.train_orientation()
        self.train_head()

    def run_body(self, img_dir, out_dir, batch_size=20, n_random=None):
        ext = BodyExtractor()
        self.body_model.load()
        cr = BodyCropper(self.body_model, ext, img_dir, out_dir)
        cr.run(batch_size, n_random)

    def run_head(self, img_dir, out_dir, batch_size=20, n_random=None):
        ext = HeadExtractor()
        self.head_model.load()
        cr = HeadCropper(self.head_model, ext, img_dir, out_dir)
        cr.run(batch_size, n_random)
