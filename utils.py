import numpy as np
import os
import cPickle as pickle
import lasagne

import config


def write_lasagne_model(model, filename):
    data = lasagne.layers.get_all_param_values(model)
    filename = os.path.join(config.ROOT, 'cache', filename)
    filename = '%s.%s' % (filename, 'model')
    with open(filename, 'w') as f:
        pickle.dump(data, f)


def read_lasagne_model(model, filename):
    filename = os.path.join(config.ROOT, 'cache', '%s.%s' % (filename, 'model'))
    with open(filename, 'r') as f:
        data = pickle.load(f)
    lasagne.layers.set_all_param_values(model, data)


def relative_squared_error(a, b):
    """
    Computers squared error scaled by a constant
    (size of the vector). This doesn't change the behaviour
    of the function but allows to compare different mask sizes.
    """
    return ((a - b) ** 2) #/ b.shape[1]


def euclidean(x1, y1, x2, y2):
    return np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
