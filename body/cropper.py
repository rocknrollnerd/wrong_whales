import os
import numpy as np
from PIL import Image, ImageDraw
import copy
from sklearn.decomposition import RandomizedPCA, PCA
from sklearn.preprocessing import MinMaxScaler
import matplotlib.patches as patches
from matplotlib.path import Path
import matplotlib.pyplot as plt
import cStringIO as StringIO
from skimage.transform import rotate

from body.datamaker.bbox_processing import rect_path
import config


class BodyCropper(object):
    name = 'body'

    def __init__(self, model, extractor, img_dir, out_dir):
        self.model = model
        self.img_dir = img_dir
        self.out_dir = out_dir
        self.extractor = extractor
        self.cache_dir = os.path.join(config.ROOT, 'cache')

    @property
    def data_path(self):
        return os.path.join(self.cache_dir, 'data_{}.npy'.format(self.name))

    @property
    def target_path(self):
        return os.path.join(self.cache_dir, 'target_{}.npy'.format(self.name))

    @property
    def datamean_path(self):
        return os.path.join(self.cache_dir, 'datamean_{}.npy'.format(self.name))

    @property
    def datastd_path(self):
        return os.path.join(self.cache_dir, 'datastd_{}.npy'.format(self.name))

    def project_onto_axes(self, center, ax1, ax2, pts, im):
        ox, oy = center

        def project_pt(pt):
            # print np.dot(ax1, pt)
            pr1 = np.dot(ax1, (pt - center)) / (np.linalg.norm(ax1) ** 2)
            pr2 = np.dot(ax2, (pt - center)) / (np.linalg.norm(ax2) ** 2)
            return pr1, pr2

        dists1 = []
        weights = []
        dists2 = []
        for pt in pts:
            d1, d2 = project_pt(pt)
            dists1.append(d1)
            dists2.append(d2)
            x, y = pt
            weights.append(im[y, x])
        return np.array(dists1), np.array(dists2), np.array(weights)

    def fit_bbox_to_mask(self, im):
        # make a tresholded version for pca
        threshold = np.mean(im)
        threshed_im = im.copy()
        threshed_im[threshed_im < threshold] = 0
        threshed_im[threshed_im >= threshold] = 1

        xs = []
        ys = []
        weights = []

        for i in xrange(im.shape[0]):
            for j in xrange(im.shape[1]):
                if im[i, j] > 0:
                    xs.append(j)
                    ys.append(i)
                    weights.append(im[i, j])

        if not xs:
            return None
        ox = np.average(xs, weights=weights)
        oy = np.average(ys, weights=weights)

        # plt.imshow(im, cmap='gray')
        # patch = patches.Circle((ox, oy), radius=3, fc='y')
        # plt.gca().add_patch(patch)
        # plt.show()

        # then time to pca.
        # use thresholded version

        h, w = im.shape
        im = MinMaxScaler().fit_transform(im.ravel()).reshape(h, w)

        pts = []
        for i in xrange(threshed_im.shape[0]):
            for j in xrange(threshed_im.shape[1]):
                if threshed_im[i, j] == 1:
                    # if im[i, j] > np.random.rand():
                    pts.append([j, i])
        pts = np.vstack(pts)
        pca = PCA(n_components=2)
        pca.fit(pts)
        center = np.array([ox, oy])
        v1 = pca.components_[0]
        v2 = pca.components_[1]

        ox_axis = np.array([1., 0.])
        cos_alpha = np.dot(ox_axis, v1) / np.linalg.norm(ox_axis) / np.linalg.norm(v1)
        alpha = np.arccos(cos_alpha)

        # YE OLDE CODE //

        # project pts to new axes
        dists_1, dists_2, weights = self.project_onto_axes(center, v1, v2, pts, im)
        # # mass = np.sum(weights)
        # mass = len(pts)
        # print 'mass', mass
        # for cand_w in np.linspace(1, im.shape[0], im.shape[0] * 10):
        #     idx = np.abs(dists_2) < cand_w / 2.
        #     # selected_mass = np.sum(weights[idx])
        #     selected_mass = np.count_nonzero(idx)
        #     print 'width', cand_w, 'mass percentage', selected_mass / float(mass), 'mass', selected_mass
        #     if selected_mass / float(mass) >= 0.8:
        #         bbox_h = cand_w
        #         bbox_w = bbox_h * 3.
        #         break

        bbox_w = np.max(dists_1) - np.min(dists_1)
        # bbox_h = np.max(dists_2) - np.min(dists_2)
        bbox_h = bbox_w / 3.

        coords = self.bbox_to_coords((bbox_w, bbox_h, ox, oy, -alpha))
        plt.imshow(im, cmap='gray')
        path = Path(coords + [coords[-1]], rect_path)
        patch = patches.PathPatch(path, lw=2, ec='y', fill=False)
        plt.gca().add_patch(patch)
        plt.show()

        return bbox_w, bbox_h, ox, oy, -alpha

        # // YE OLDE CODE

        # now let's try to find width and height in a different way
        # fix aspect ratio (0.3 maybe?), iterate across possible width/height combinations
        # and measure the following metric: (sum of mask pixels inside the rect) / (rect area)
        # which is in (0, 1)
        # return maximum settings

        aspect_ratio = 0.3
        metrics = []
        vals = []
        # print 'imshape', im.shape
        # im -= np.mean(im)
        for w in np.linspace(1, im.shape[0], im.shape[0]):
        # for w in (5, 10, 15):
            h = w * aspect_ratio
            print w, h
            vals.append((w, h))
            coords = self.bbox_to_coords((w, h, ox, oy, -alpha))

            plt.gca().set_axis_bgcolor((0, 0, 0))

            path = Path(coords + [coords[-1]], rect_path)
            patch = patches.PathPatch(path, facecolor='none', lw=0)
            plt.gca().add_patch(patch)

            plt.xticks(())
            plt.yticks(())
            plotted_im = plt.imshow(im, cmap='gray')
            plotted_im.set_clip_path(patch)
            # plt.show()

            io = StringIO.StringIO()
            plt.savefig(io, bbox_inches='tight', pad_inches=0)
            plt.clf()
            io.seek(0)

            pil_im = Image.open(io)
            np_im = (np.asarray(pil_im) / 255.).mean(axis=-1) / 4.
            # np_im[np_im == np.mean(np_im)] = 0.
            print np.min(np_im), np.max(np_im), np.mean(np_im)
            np_im -= np.mean(np_im)

            metrics.append(np.sum(np_im))
            print 'metr', metrics[-1]

        print 'DUN DUN DUN'
        max_metric = np.max(metrics)
        print max_metric, metrics.index(max_metric)
        # raw_input()
        w_opt, h_opt = vals[metrics.index(max_metric)]

        coords = self.bbox_to_coords((w_opt, h_opt, ox, oy, -alpha))
        plt.imshow(im, cmap='gray')
        path = Path(coords + [coords[-1]], rect_path)
        patch = patches.PathPatch(path, lw=2, ec='y', fill=False)
        plt.gca().add_patch(patch)
        plt.show()

        return w_opt, h_opt, ox, oy, -alpha

    def rescale_bbox(self, bbox, original_dim, resize_dim):
        width, height, ox, oy, angle = bbox
        width = (width / original_dim) * resize_dim
        height = (height / original_dim) * resize_dim
        ox = (ox / original_dim) * resize_dim
        oy = (oy / original_dim) * resize_dim
        return width, height, ox, oy, angle

    def rotate_2d_point(self, pt, pivot, angle):
        x, y = pt
        px, py = pivot
        x -= px
        y -= py
        xnew = x * np.cos(angle) - y * np.sin(angle)
        ynew = x * np.sin(angle) + y * np.cos(angle)
        xnew += px
        ynew += py
        return xnew, ynew

    def rotate_bbox_with_image(self, bbox, angle, pivot):
        width, height, ox, oy, bbox_angle = bbox
        ox_new, oy_new = self.rotate_2d_point((ox, oy), pivot, -angle)
        angle_new = bbox_angle + angle
        return width, height, ox_new, oy_new, angle_new

    def bbox_to_coords(self, bbox):
        """
        Transforms bbox from width, height, ox, oy, angle
        to 4 coords points
        """
        width, height, ox, oy, angle = bbox
        pts = [
            (ox - width / 2., oy - height / 2.),
            (ox - width / 2., oy + height / 2.),
            (ox + width / 2., oy + height / 2.),
            (ox + width / 2., oy - height / 2.),
        ]
        rotated_pts = [self.rotate_2d_point(pt, (ox, oy), -angle) for pt in pts]
        # scaled_pts = sum([[x, y] for x, y in rotated_pts], [])
        return rotated_pts

    def run(self, batch_size, n_random):
        # prepare a list of files
        files = sorted(os.listdir(self.img_dir))

        # if `n_random` is chosed, take n random images from the source dir
        if n_random:
            files = np.random.choice(files, n_random)

        # create output dir if nonexistent
        if not os.path.isdir(self.out_dir):
            os.mkdir(self.out_dir)

        num_batches = len(files) / batch_size + 1
        c = 0
        for b in xrange(num_batches):
            batch_data = np.zeros((batch_size, config.INPUT_BODY_SHAPE[1], config.INPUT_BODY_SHAPE[0], 3))
            batch_fnames = []
            batch_originals = []
            for i, fname in enumerate(files[b * batch_size: (b + 1) * batch_size]):
                batch_fnames.append(fname)
                path = os.path.join(self.img_dir, fname)
                ext = path.split('.')[-1]

                original_pil_img = Image.open(path)
                original_pil_img = self.extractor.pad_to_square(original_pil_img)
                orig_w, orig_h = original_pil_img.size
                batch_originals.append(original_pil_img)

                resized_img = copy.deepcopy(original_pil_img)
                resized_img.thumbnail((config.INPUT_BODY_SHAPE[1], config.INPUT_BODY_SHAPE[0]), Image.ANTIALIAS)

                im = np.float32(np.asarray(resized_img)) / 255.
                batch_data[i] = im

            datamean = np.load(self.datamean_path)
            datastd = np.load(self.datastd_path)

            batch_data = batch_data.reshape(batch_data.shape[0], 3, config.INPUT_BODY_SHAPE[1], config.INPUT_BODY_SHAPE[0])
            batch_data -= datamean
            batch_data /= (datastd + 1e-6)
            batch_data = np.float32(batch_data)

            preds = self.model.predict(batch_data)
            for i in xrange(batch_size):
                if i >= len(batch_originals):
                    break
                pred = preds[i]
                bbox = self.fit_bbox_to_mask(pred.reshape(config.MASK_BODY_SHAPE[1], config.MASK_BODY_SHAPE[0]).copy())
                if bbox is None:
                    print 'Warning: whale not detected', batch_fnames[i]
                    continue
                original_pil_img = batch_originals[i]
                orig_w, orig_h = original_pil_img.size
                rbbox = self.rescale_bbox(bbox, config.MASK_BODY_SHAPE[1], orig_h)
                w, h, ox, oy, alpha = rbbox

                rotated_img = original_pil_img.rotate(np.degrees(-alpha))
                newbbox = self.rotate_bbox_with_image(rbbox, -alpha, (orig_w / 2., orig_h / 2.))

                w, h, ox, oy, alpha = newbbox
                pts = self.bbox_to_coords(newbbox)
                xs = [pt[0] for pt in pts]
                ys = [pt[1] for pt in pts]
                left, upper, right, lower = int(np.min(xs)), int(np.min(ys)), int(np.max(xs)), int(np.max(ys))
                cropped = rotated_img.crop((left, upper, right, lower))
                fname = batch_fnames[i]
                c += 1
                print c, 'of', len(files), fname
                cropped.save(os.path.join(self.out_dir, fname))
