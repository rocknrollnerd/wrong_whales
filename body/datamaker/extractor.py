import os
from PIL import Image
import numpy as np
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from matplotlib.path import Path
import cStringIO as StringIO
import json

from body.datamaker.bbox_processing import rotate_2d_point, rect_path, Bboxer
from body.datamaker.augmentation import augment
import config

"""
Does the datamaking and ties together
all the processing performed with the initial
dataset: loading, resizing, drawing masks, augmentations
"""


class BodyExtractor(object):
    RESIZE_DIM = 600
    OUT_DIM = 100
    name = 'body'
    OUT_DIR = 'raw_datasets/{}'.format(name)

    def __init__(self):
        self.img_dir = config.DATASET_BODY
        self.out_img_dir = os.path.join(config.ROOT, self.OUT_DIR, 'images')
        self.out_mask_dir = os.path.join(config.ROOT, self.OUT_DIR, 'masks')

    def pad_to_square(self, pil_im, bbox=None):
        """
        Pads image to square with black pixels.
        Also requires to correspondently change bbox.
        """
        # width, height, ox, oy, alpha = bbox
        if bbox:
            bbox = bbox.copy()
        w, h = pil_im.size

        # pad zeros
        im = np.asarray(pil_im)
        if w > h:
            pad_size = int((w - h) / 2.)
            if bbox:
                bbox.oy += pad_size
            im = np.pad(im, ((pad_size, pad_size), (0, 0), (0, 0)), 'constant', constant_values=0)
            h += pad_size * 2
        else:
            pad_size = int((h - w) / 2.)
            if bbox:
                bbox.ox += pad_size
            im = np.pad(im, ((0, 0), (pad_size, pad_size), (0, 0)), 'constant', constant_values=0)
            w += pad_size * 2

        # cut extra pixels
        h, w, _ = im.shape
        if h > w:
            im = im[:w, :, :]
        elif h < w:
            im = im[:, :h, :]
        h, w, _ = im.shape
        pil_im = Image.fromarray(im)
        if bbox:
            return pil_im, bbox
        else:
            return pil_im

    def get_mask(self, bbox, original_dim):
        """
        Makes a black-and-white-ish mask with bbox area
        filled with white.
        Cannot find how to make oriented rectangles in PIL,
        hence this super-ugly matplotlib solution.
        """
        im = np.zeros((original_dim, original_dim))
        h, w = im.shape
        plt.imshow(im, extent=[0, w, h, 0])
        # bbox = self.rescale_bbox(bbox, w, self.RESIZE_DIM)
        # width, height, ox, oy, angle = bbox
        bbox = bbox.copy()

        plt.xticks(())
        plt.yticks(())

        ax = plt.gca()
        pts = [
            (bbox.ox - bbox.width / 2., bbox.oy - bbox.height / 2.),
            (bbox.ox - bbox.width / 2., bbox.oy + bbox.height / 2.),
            (bbox.ox + bbox.width / 2., bbox.oy + bbox.height / 2.),
            (bbox.ox + bbox.width / 2., bbox.oy - bbox.height / 2.),
            (bbox.ox - bbox.width / 2., bbox.oy - bbox.height / 2.),  # for Path
        ]
        rotated_pts = [rotate_2d_point(pt, (bbox.ox, bbox.oy), -bbox.alpha) for pt in pts]
        path = Path(rotated_pts, rect_path)
        patch = patches.PathPatch(path, fill=True, lw=0, fc='w', ec='w', zorder=10)
        ax.add_patch(patch)

        buff = StringIO.StringIO()
        plt.savefig(buff, dpi=200, bbox_inches='tight', pad_inches=0)  # dpi needs manual adjustment
        buff.seek(0)
        pil_im = Image.open(buff).convert('L')
        pil_im.thumbnail((self.RESIZE_DIM, self.RESIZE_DIM))
        plt.clf()
        return pil_im

    def extract_annotations(self, verbose=True):
        c = 0
        with open(config.ANNOTATIONS_BODY, 'r') as f:
            annotations = json.loads(f.read())
        for data in annotations[::-1]:
            fn = data['filename']
            ffn = fn.split('/')[-1]
            ext = ffn.split('.')[-1]
            path = os.path.join(self.img_dir, ffn)
            fn = fn[3:] if fn.startswith('../') else fn
            if verbose:
                print 'Processing {}'.format(fn)
            if data['annotations']:
                poly = data['annotations'][0]
                bbox = Bboxer.fit_bbox(poly)
                pil_im = Image.open(path)
                pil_im, bbox = self.pad_to_square(pil_im, bbox)
                w, h = pil_im.size
                pil_mask = self.get_mask(bbox, w)
                pil_im.thumbnail((self.RESIZE_DIM, self.RESIZE_DIM), Image.ANTIALIAS)

                augmentations = augment(pil_im, pil_mask)
                for im, mask in augmentations:
                    im.thumbnail((self.OUT_DIM, self.OUT_DIM), Image.ANTIALIAS)
                    mask.thumbnail((self.OUT_DIM, self.OUT_DIM), Image.ANTIALIAS)
                    im.save(os.path.join(self.out_img_dir, '{}.{}'.format(c, ext)))
                    mask.save(os.path.join(self.out_mask_dir, '{}.{}'.format(c, ext)))
                    c += 1
