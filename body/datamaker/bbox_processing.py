import json
import math
import os
import sys
###
from PIL import Image, ImageOps
import numpy as np
import matplotlib.image as mpimg
import matplotlib.patches as patches
from matplotlib import transforms
import matplotlib.pyplot as plt
import pickle
from matplotlib.path import Path
import cStringIO as StringIO
from pilkit.processors import Crop
import random

from utils import euclidean

"""
Handles bounding box processing: fitting rectangulars
to polygons, rotating and scaling the boxes.
"""

rect_path = (
    Path.MOVETO,
    Path.LINETO,
    Path.LINETO,
    Path.LINETO,
    Path.CLOSEPOLY
)


def rotate_2d_point(pt, pivot, angle):
    x, y = pt
    px, py = pivot
    x -= px
    y -= py
    xnew = x * np.cos(angle) - y * np.sin(angle)
    ynew = x * np.sin(angle) + y * np.cos(angle)
    xnew += px
    ynew += py
    return xnew, ynew


class OrientedBbox(object):
    """
    Bbox that supports orientation and has `alpha`
    parameter.
    """

    def __init__(self, width, height, ox, oy, alpha):
        self.width = width
        self.height = height
        self.ox = ox
        self.oy = oy
        self.alpha = alpha

    def copy(self):
        return OrientedBbox(self.width, self.height, self.ox, self.oy, self.alpha)


class Bboxer(object):

    @classmethod
    def fit_bbox(cls, poly):
        xn = [float(x) for x in poly['xn'].split(';')]
        yn = [float(y) for y in poly['yn'].split(';')]

        # first find center
        ox = np.mean(xn)
        oy = np.mean(yn)

        # then the longest median line
        # median points are between pts1 and 2, and pts3 and 4
        if xn[0] < xn[2]:
            xmedian1 = np.mean([xn[0], xn[1]])
            ymedian1 = np.mean([yn[0], yn[1]])
            xmedian2 = np.mean([xn[2], xn[3]])
            ymedian2 = np.mean([yn[2], yn[3]])
        else:
            xmedian1 = np.mean([xn[2], xn[3]])
            ymedian1 = np.mean([yn[2], yn[3]])
            xmedian2 = np.mean([xn[0], xn[1]])
            ymedian2 = np.mean([yn[0], yn[1]])

        # median1 is left, median2 is right

        if ymedian1 < oy:
            k = -1
        else:
            k = 1

        width = euclidean(xmedian1, ymedian1, xmedian2, ymedian2)

        # use it to calculate rotation angle
        c = width
        a = euclidean(xmedian1, ymedian2, xmedian2, ymedian2)
        alpha = np.arccos(a / c) * k

        # estimate heights by another median
        xmedian1 = np.mean([xn[0], xn[3]])
        ymedian1 = np.mean([yn[0], yn[3]])
        xmedian2 = np.mean([xn[1], xn[2]])
        ymedian2 = np.mean([yn[1], yn[2]])
        height = euclidean(xmedian1, ymedian1, xmedian2, ymedian2)

        width *= 1.1
        height *= 1.1

        return OrientedBbox(width, height, ox, oy, alpha)

    @classmethod
    def rescale_bbox(cls, bbox, original_dim, resize_dim):
        bbox = bbox.copy()
        bbox.width = (bbox.width / original_dim) * resize_dim
        bbox.height = (bbox.height / original_dim) * resize_dim
        bbox.ox = (bbox.ox / original_dim) * resize_dim
        bbox.oy = (bbox.oy / original_dim) * resize_dim
        return bbox
