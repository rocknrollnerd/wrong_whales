import numpy as np


def augment(pil_im, pil_mask, num_rotations=50):
    # augmentation time!
    # rotating
    result = []
    # for rot in xrange(0, 360, 90):
    for j in xrange(num_rotations):
        rot = np.random.randint(0, 360)
        rot_im = pil_im.rotate(rot)
        rot_mask = pil_mask.rotate(rot)

        # random crops with different scales
        # for i in xrange(3):
        #     crop_size = np.random.randint(self.OUT_DIM, self.RESIZE_DIM)
        #     remained = self.RESIZE_DIM - crop_size

        #     if remained in (0, 1):
        #         start_x = start_y = 0
        #     else:
        #         start_x = np.random.randint(0, remained / 2)
        #         start_y = np.random.randint(0, remained / 2)

        #     cropped_mask = rot_mask.crop((start_x, start_y, start_x + crop_size, start_y + crop_size))
        #     cropped_im = rot_im.crop((start_x, start_y, start_x + crop_size, start_y + crop_size))

        cropped_im = rot_im
        cropped_mask = rot_mask
        result.append((cropped_im, cropped_mask))
    return result
